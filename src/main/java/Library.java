import dao.BookDao;
import dao.BookDaoSqLite;
import domain.Book;

import java.util.Scanner;

public class Library {
    private BookDao bookDao;

    public BookDao getBookDao() {
        return bookDao;
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public static void main(String[] args) {
        Library library = new Library();
        library.setBookDao(new BookDaoSqLite());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj tytuł");
        String title = scanner.nextLine();
        System.out.println("Podaj autora");
        String author = scanner.nextLine();
        System.out.println("Podaj liczbę stron");
        int pages = scanner.nextInt();
        library.getBookDao().addBook(new Book(title,author,pages));
    }
}
