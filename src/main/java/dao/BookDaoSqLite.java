package dao;
import domain.Book;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class BookDaoSqLite implements BookDao {
    private Connection connection;

    public BookDaoSqLite() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:library.db");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        createTable();
    }

    private void createTable() {
        String sql = "CREATE TABLE IF NOT EXISTS Books("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "title TEXT NOT NULL, "
                + "author TEXT NOT NULL, "
                + "pages INTEGER DEFAULT 0"
                + ")";

        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addBook(Book book) {
        String title = book.getTitle();
        String author = book.getAuthor();
        int pages = book.getPages();

        String sql = "INSERT INTO Books(title, author, pages) VALUES(" +
                "'" + title + "', '" + author + "', " + pages + ")";

        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //String query = "SELECT id FROM Books WHERE title = '" + ...
        //book.setId(id)
    }

    @Override
    public void removeBook(Book book) {
        int id = book.getId();

        String sql = "DELETE FROM Books WHERE id = " + id;
        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Book> findAll() {
        return null;
    }
}